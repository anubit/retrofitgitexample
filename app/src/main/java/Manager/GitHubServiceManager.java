package Manager;

import android.content.Context;
import com.squareup.otto.Bus;

import api.GitHubServiceClient;


/**
 * Created by Anu on 3/17/16.
 */

// this will be the central hub for receiving and sending events between controllers
// and background network activity
// This class has a reference to context, bus and the client(retrofit)
public class GitHubServiceManager {

    private Context mContext;
    private Bus mBus; // reference variable to Otto bus
    private GitHubServiceClient gitHubServiceClient; // reference to our client class where
    // the retrofit is built.

    // we are passing in context so we can use for shared preferences  or system services
    public  GitHubServiceManager(Context context, Bus bus) {
        this.mContext = context;
        this.mBus = bus;
        gitHubServiceClient = GitHubServiceClient.getClient();

    }


}
