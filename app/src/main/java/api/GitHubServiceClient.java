package api;

import com.squareup.okhttp.OkHttpClient;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by Anu on 3/18/16.
 */

// This class will implement the retrofit
// Retrofit uses a class called RestAdapter to handle networking. It is an interface to a
// REST api. Retrofit uses Builder or Factory pattern to create your adapter to handle
// networking.
// You can use a special client called client with OKHttpClient to handle timeouts, download
// sizes, other troublesome network calls automatically

// There is only one  adapter for our networking calls
// following singleton pattern

// Retrofit.Builder is an inner nested class inside RestAdapter, has methods
// like .build,.setClient(client) etc

// If the api involves OAuth you need to set the requestInterceptor


// My observation: good idea to have this client to protect encapsulation
public class GitHubServiceClient {

    private static final String BASE_URL = "https://api.github.com" ;
    private static final String API_KEY="***************************";
    private static final String API_URL = BASE_URL + API_KEY;

    private static GitHubServiceClient mGitHubServiceClient;

    public static GitHubServiceClient getClient() {
        if (mGitHubServiceClient == null)
            mGitHubServiceClient = new GitHubServiceClient();

        return mGitHubServiceClient;
    }

    // Constructor you build the Retrofit
    private GitHubServiceClient() {
        OkHttpClient client = new OkHttpClient();
        // Builds a new RetrofitAdapter instance.
        Retrofit retrofitAdapter = new Retrofit.Builder()
                .client(client)
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

}
