package ex.retrofitgitexample.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
//import javax.annotation.Generated;

import okio.Source;

/**
 * Created by Anu on 3/14/16.
 */
// serialize: from java to json
//@expose: will do both deserialize as well as serialize
//@Generated("org.jsonschema2pojo")
public class Datum {

   /* @SerializedName("name")
    @Expose
    private String name;*/

    @SerializedName("owner")
    @Expose
    private ex.retrofitgitexample.model.User owner;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    /*
    @SerializedName("stargazers_count")
    @Expose
    private int numStars;
    @SerializedName("watchers_count")
    @Expose
    private int numReplies;*/

    @SerializedName("body")
    @Expose
    private String description;

    @Expose
    private String title;

    @Expose
    private String id;
    /*
    @Expose
    private int forks;*/
    @SerializedName("open_issues")
    @Expose
    private int openIssues;

    /**
     *
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     *     The numStars
     */
  /*  public int getNumStars() {
        return numStars;
    }*/

    /**
     *
     * @param numStars
     *     The num_stars
     */
  /*  public void setNumStars(int numStars) {
        this.numStars = numStars;
    }*/

    /**
     *
     * @return
     *     The numReplies
     */
  /*  public int getNumReplies() {
        return numReplies;
    }*/

    /**
     *
     * @param numReplies
     *     The num_replies
     */
  /*  public void setNumReplies(int numReplies) {
        this.numReplies = numReplies;
    }*/


    /**
     *
     * @return
     *     Description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     *     The text
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     *     forks
     */
   /* public int getForks() {
        return forks;
    }
*/
    /**
     *
     * @param forks
     *     The num_reposts
     */
  /*  public void setNumReposts(int forks) {
        this.forks = forks;
    } */

    /**
     *
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     *     The user
     */
    public ex.retrofitgitexample.model.User getOwner() {
        return owner;
    }

    /**
     *
     * @param owner
     *     The user
     */
   public void setUser(ex.retrofitgitexample.model.User owner) {
        this.owner = owner;
    }

    /**
     *
     * @return
     *     openIssues
     */
    public int getOpenIssues() {
        return openIssues;
    }

    /**
     *
     * @param openIssues
     *     The openIssues
     */
    public void setOpenIssues(int openIssues) {
        this.openIssues = openIssues;
    }



}