package ex.retrofitgitexample.model;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anu on 3/17/16.
 */

// This class is the response for github api request
// You have defined a list to accept the responselist

public class GitHubResponse {
    @SerializedName("items")
    List<ex.retrofitgitexample.model.Datum> responseList;

    public static String DATE_FORMAT = "yyyy-MM-dd";
    //2010-02-08T13:20:56Z

    // public constructor is necessary for collections
    // To avoid null pointer exceptions intialize these objects in
    // the empty constructor.
    public GitHubResponse(){
        responseList = new ArrayList<ex.retrofitgitexample.model.Datum>();
        //responseList = getResponseList();
        Log.d("GitHubReponseclass", "the response list = " + responseList);
        Log.d("GitHubResponseClass", " the list number = " + responseList.size());
    }


    public List<ex.retrofitgitexample.model.Datum> getResponseList()
    {

        return responseList;
    }

    /*  This method is used if the json response is in the string form,
     *  fromJson method takes the json response and the second
        parameter is the class that should be mapped
     */
    public static GitHubResponse parseJson(String response){
        GsonBuilder gsonBuilder = new GsonBuilder();
        // map from string to Date object directly: specify the date format
        gsonBuilder.setDateFormat(DATE_FORMAT);
        Gson gson = gsonBuilder.create();

        GitHubResponse gitHubResponse = gson.fromJson(response, GitHubResponse.class);
        return gitHubResponse;
    }
}
