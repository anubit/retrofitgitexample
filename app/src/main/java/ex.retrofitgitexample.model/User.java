package ex.retrofitgitexample.model;


/**
 * Created by Anu on 3/15/16.
 */

// gson creates POJO objects. plan old java objects
// all you need to mention is the serialized name
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class User {

    @SerializedName("login")
    @Expose
    private String name;

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("avatar_url")
    private String avatarUrl;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
}
