package application;

import android.app.Application;

import Bus.BusProvider;
import com.squareup.otto.Bus;
import Manager.GitHubServiceManager;

/**
 * Created by Anu on 3/17/16.
 */

// This class registers our bus and manager class
public class GitHubApplication extends Application {

    // reference to the manager class
    private GitHubServiceManager mgithubServiceManager;
    // getting the bus object from BusProvider class
    private Bus mBus = BusProvider.getInstance();

    @Override
    public void onCreate() {
        super.onCreate();
        // we are cpassing the application context and the bus to the manager
        mgithubServiceManager = new GitHubServiceManager(this,mBus);
        // register the manager to the bus
        mBus.register(mgithubServiceManager);
        mBus.register(this);
    }
}
