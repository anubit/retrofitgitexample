package UI;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ex.retrofitgitexample.R;
import ex.retrofitgitexample.activity.MainActivity;
import ex.retrofitgitexample.model.Datum;

/**
 * Created by Anu on 3/29/16.
 */

/* Create the basic adapter extending from RecyclerView.Adapter
 * Note that we specify the custom ViewHolder which gives us access to our views
 */
public class GHBRecyclerAdapter extends RecyclerView.Adapter<GHBRecyclerAdapter.ViewHolder>{

    /** CLASS VARIABLES ________________________________________________________________________ **/

    // ACTIVITY VARIABLES for Picasso
    private Activity activity; // References the attached activity.

    // LIST VARIABLES
    private List<Datum> postDataList = Collections.emptyList();

    // LOGGING VARIABLES
    private static final String LOG_TAG = GHBRecyclerAdapter.class.getSimpleName();


    /** INITIALIZATION METHODS _________________________________________________________________ **/
    //CONSTRUCTOR
    // Pass in the data array into the constructor
    public GHBRecyclerAdapter(List<Datum> dataList) {
        if (dataList== null)
        {
            Log.d(LOG_TAG,"empty data list");

        }
        else this.postDataList = dataList;
    }

    /*  Provide a direct reference to each of the views within a data item
     *  Used to cache the views within the item layout for fast access
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        /*  Your holder should contain all the member variables
         * for any view that will be set as you render a row
         */
        public TextView tvTitle,tvLoginName;
        public ImageView imgVAvatar;

        /* We also create a constructor that accepts the entire item row
         * and does the view lookups to find each subview
         */
        public ViewHolder(View itemView) {
            /* Stores the itemView in a public final member variable that can be used
             * to access the context from any ViewHolder instance.
             */
            super(itemView);

            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            tvLoginName = (TextView) itemView.findViewById(R.id.tvLoginName);
            imgVAvatar = (ImageView) itemView.findViewById(R.id.imgViewAvatar);
        }

    }

    // onCreateViewHolder: This method is called when the custom ViewHolder needs to be initialized.
    // The layout of each item of the RecyclerView is inflated using xml LayoutInflater, passing the
    // output to the constructor of the custom ViewHolder, basically creating a custom view holder
    @Override
    public GHBRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // get parent context
        Context context = parent.getContext();
        //create an inflater
        LayoutInflater inflater = LayoutInflater.from(context);

        //inflate the custom layout
        View dataListView = inflater.inflate(R.layout.item_list_issues, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(dataListView);
        return viewHolder;
    }

    // onBindViewHolder(): Overrides the onBindViewHolder to specify the contents of each item of
    // the RecyclerView. This method is similar to the getView method of a ListView's adapter.
    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(GHBRecyclerAdapter.ViewHolder holder, int position) {
        // Set item views based on the data model
        holder.tvLoginName.setText(postDataList.get(position).getOwner().getName());
        holder.tvTitle.setText(postDataList.get(position).getTitle());

        Log.d(LOG_TAG, "onBindViewHolder(): Poster Name: " + postDataList.get(position).getOwner().getName());
        Log.d(LOG_TAG, "onBindViewHolder(): Title Description: " + postDataList.get(position).getTitle());


        // Retrieves the avatar image URL at the referenced position.
        String avatarImage = null;

        try {
            avatarImage = postDataList.get(position).getOwner().getAvatarUrl();
        }

        catch (NullPointerException e) {
            Log.e(LOG_TAG, "onBindViewHolder(): Null pointer exception encountered while attempting to retrieve the avatar image URL.");
        }


        // Loads the referenced image into the ImageView object using Picasso
        if (avatarImage != null) {

            Picasso.with(activity)
                    .load(avatarImage)
                    .into(holder.imgVAvatar);
        }

        // If no referenced image exists, the application icon is set instead.
        else {

            Picasso.with(activity)
                    .load(R.mipmap.ic_launcher)
                    .into(holder.imgVAvatar);
        }



    }

    // Return the total count of items
    @Override
    public int getItemCount() {
//        Log.d("adapter", " post data list in getCount = " + postDataList.size());
        return postDataList == null ? 0 : postDataList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}



