package ex.retrofitgitexample.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import Bus.BusProvider;
import Connection.GHBConnectivity;

import com.squareup.okhttp.OkHttpClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import Interface.MyRetrofitEndPointInterfaceGitHub;
import UI.GHBRecyclerAdapter;
import butterknife.Bind;

import butterknife.ButterKnife;
import ex.retrofitgitexample.R;
import ex.retrofitgitexample.model.Datum;
import ex.retrofitgitexample.model.GitHubResponse;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

import com.squareup.otto.Bus;

// 1. Demonstrate without otto
// 2. retrofit with AsyncTask
// 3. demonstrate with client and manager
// 4. Otto with Retrofit

public class MainActivity extends AppCompatActivity {

    /** LIST VARIABLES______________________________________*/
    private List<Datum> postListResult;
    private String searchTerm, language;
    /**LOGGING VARIABLES_______________________*/
    private static final String tag = MyRetrofitEndPointInterfaceGitHub.class.getSimpleName();
    /*ASYNCTASK VARIABLES______________________________*/
    private GHSQueryTask queryTask; // references the AsyncTask

    private Bus mBus = BusProvider.getInstance();

    private GHBRecyclerAdapter recyclerViewAdapter;
    private LinearLayoutManager layoutManager;

    //VIEW INJECTION VARIABLES
    @Bind(R.id.etSearch)
    EditText etSearch;
    @Bind(R.id.btnFetch)
    Button btnFetch;
  /*  @Bind(R.id.toolbar)
    Toolbar toolbar;*/
    @Bind((R.id.rvOpenIssues))
    RecyclerView recyclerView;
   /* @Bind(R.id.fab)
    FloatingActionButton fab;*/

    /** ACTIVITY LIFECYCLE METHODS _____________________________________________________________ **/

    // onCreate(): The first method that is run when the activity is created.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(tag, "inside onCreate()");

        setupLayout();
        setupListener();


    }

    // onStop(): This method runs when the screen is no longer visible.
    @Override
    public void onStop() {
        super.onStop();

        // If the AsyncTask is still running in the background, it is cancelled at this point.
        if (null != queryTask) {
            if (queryTask.getStatus() == AsyncTask.Status.RUNNING) {
                queryTask.cancel(true); // Cancels the AsyncTask operation.
                Log.d(tag, "onStop(): AsyncTask has been cancelled.");
            }
        }
    }

    /** LAYOUT METHODS _________________________________________________________________________ **/

    // setupLayout(): Sets up the layout for the activity.
    private void setupLayout() {
//        setSupportActionBar(toolbar);
        Log.d(tag, "inside setupLayout()");
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this); // ButterKnife view injection initialization
        postListResult = new ArrayList<>();
        setUpRecyclerView();
       // setRecyclerList(postListResult);
       // recyclerView.setAdapter(new GHBRecyclerAdapter(postListResult));

    }

    private void setupListener() {
      /*  fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (etSearch.getText().toString().trim().isEmpty()) {
                    etSearch.setError("Search is empty");
                    etSearch.requestFocus();

                } else {
                    searchTerm = etSearch.getText().toString();
                    Log.d(tag," search Term after entered on the edit text = "+ searchTerm);
                    
                }

            }
        });
        language = "java";
        /* on click of fetch button, retrieve the open issue via AsyncTask________________________*/
        btnFetch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // on click of fetch send an event - Otto

                // ASYNCTASK INITIALIZATION:
                queryTask = new GHSQueryTask();
                queryTask.execute(); // Executes the AsyncTask.

                /*
                mBus.post(new GitHubService() {
                    @Override
                    public void getLatestOpenIssues(Callback<GitHubResponse> callback) {

                    }
                })
            }*/
            }
        });
    }

    // updateView(): Updates the layout view after the GHSQueryTask has completed.
    private void updateView(Boolean postsRetrieved) {

        //progressIndicator.setVisibility(View.GONE); // Hides the progress indicator object.
        Log.d(tag,"inside update view ");
        // Sets the list adapter for the RecyclerView object if the posts' data retrieval was
        // successful.
        if (postsRetrieved) {
            Log.d(tag, "update view - set up RecyclerView()");
          //  setUpRecyclerView(); // Sets up the RecyclerView object.
            setRecyclerList(postListResult); // Sets the adapter for the RecyclerView object.
            recyclerViewAdapter.notifyDataSetChanged();
        }
    }



    /** RETROFIT METHODS _______________________________________________________________________ **/

                // retrieveLatestPosts(): Builds a RetrofitAdapter for retrieving the latest global posts via
                // Rest API request.
                // Retrofit uses a class called RestAdapter to handle networking. It is an interface to a
                // REST api. Retrofit uses Builder or Factory pattern to create your adapter to handle
                // networking.
                // You can use a special client called client with OKHttpClient to handle timeouts, download
                // sizes, other troublesome network calls automatically

                // There is only one  adapter for our networking calls
                // following singleton pattern

                // Retrofit.Builder is an inner nested class inside RestAdapter, has methods
                // like .build,.setClient(client) etc

                // If the api involves OAuth you need to set the requestInterceptor

                // You can have the api request done here instead of doing it via manager


    /** RETROFIT METHODS _______________________________________________________________________ **/

    // retrieveLatestPosts(): Builds a RetrofitAdapter for retrieving the latest global open issues via
    // Rest API request

    private void retrieveLatestOpenIssues() {

        // THe following code is moved to GitHubServiceManager
        // I have access to the client via the githubservice manager.

        // 3. use retrofit to build the api request

        Log.d(tag,"inside retrieve open issues - first build retrofit");

        //logging is not integrated by default in Retrofit2, we need to add logging interceptor
        // OkHttp ships with this interceptor

        // Activate logging interceptor

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        // set your desired log level
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        // Retrofit 2 relies on OKhttp for network operation

        OkHttpClient client = new OkHttpClient();

        // Builds a new RetrofitAdapter instance.
        Retrofit retrofitAdapter = new Retrofit.Builder()
                .client(client)
                .baseUrl("https://api.github.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // add logging as last interceptor
      //  client.addInterceptor(logging);  // <-- this is the important line!

        Log.d(tag, " retrofit adapter =" + retrofitAdapter.baseUrl().url().toString());
        // Create a service class that calls the api request
        MyRetrofitEndPointInterfaceGitHub apiRequestService = retrofitAdapter.create(MyRetrofitEndPointInterfaceGitHub.class);
        Log.d(tag, "api request = " + apiRequestService.toString());
        //Version 1: without an intermediate interface of GithubRespone: simpler version.



        // Attempts to execute the Rest API request to retrieve the latest posts
        // we are passing the searchterms to RetrofitEndpoints
        try {
            Log.d(tag, " search term = " + etSearch.getText().toString());
            Log.d(tag," language = " + language);
            Log.d(tag, "get the response list = " +
                    apiRequestService.getLatestOpenIssues(searchTerm).execute().body().toString());

            Call<GitHubResponse> call = apiRequestService.getLatestOpenIssues(searchTerm);
            Response<GitHubResponse> response = call.execute();
            Log.d(tag,"response = "+ response.headers().toString());
            Log.d(tag,"response body list i = 0: " + response.body().getResponseList().get(0).getTitle());
            Log.d(tag,"response body list i = 1: " +  response.body().getResponseList().get(1).getTitle());
            Log.d(tag,"response size list = " + response.body().getResponseList().size());


            //GitHubResponse g = new GitHubResponse(response.body().getResponseList());

            postListResult = response.body().getResponseList();
            Log.d(tag,"results from api request = " + postListResult.toString());

            // http://stackoverflow.com/questions/32431525/using-call-enqueue-function-in-retrofit
            // onResponse is called even if there is a failure
            call.clone().enqueue(new Callback<GitHubResponse>() {
                @Override
                public void onResponse(Response<GitHubResponse> response, Retrofit retrofit) {
                    //gitHubResponse = response.body();
                    // You check for isSuccess
                    if (response.isSuccess()) {
                        Log.d(tag, "response is success");
                        return;
                    } else {
                        Log.d(tag, " response error" + response.errorBody().toString());
                        return;
                    }

                }

                @Override
                public void onFailure(Throwable t) {

                }
            });

          //  postListResult = apiRequestService.getLatestOpenIssues(searchTerm,language).execute().body()
            //        .getResponseList();

        }

        // I/O Exception handler.
        catch (IOException e) {
            Log.e(tag, "retrieveLatestPosts(): Exception occurred while trying to retrieve posts: " + e);
            e.printStackTrace();
        }

        // VERSION:2  NO otto version For otto version this part move it to manager class
        // getLatestOpenIssues() is the Interface call.
        // The prob
        /*
        Call<GitHubResponse> call = apiRequest.getLatestOpenIssues();
        call.enqueue(new Callback<GitHubResponse>() {

            @Override
            public void onResponse(Response<GitHubResponse> response, Retrofit retrofit) {
                GitHubResponse gitHubResponse = response.body();
                postListResult = gitHubResponse.getResponseList();
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });*/

    }


    // you can have api request and the call code below
    // TODO: this code has to be written in manager class?
    public void onRetrieveLatestPost(MyRetrofitEndPointInterfaceGitHub getLatestPostEvent){

    }



    /** RECYCLERVIEW METHODS ___________________________________________________________________ **/

    // we will populate a set of sample users which should be displayed in the RecyclerView.
    private void setUpRecyclerView() {
        //1.  Lookup the recyclerview in activity layout
        // done in Bind injection variable
        
        Log.d(tag,"inside set up recycler view");
        //5. create and set layout manager to position the items
        layoutManager = new LinearLayoutManager(this);
        Log.d(tag,"layour manager = " + layoutManager.toString());
        recyclerView.setLayoutManager(layoutManager);


    }

    private void setRecyclerList(List<Datum> postList){
        Log.d(tag,"inside set recycler list- set the adapter ");

        //3. create adapter and set
        recyclerViewAdapter = new GHBRecyclerAdapter(postList);
        //4.  Attach the adapter to the recyclerview to populate items
        recyclerView.setAdapter(recyclerViewAdapter);

    }

    /** ASYNC SUBCLASSES _____________________________________________**/

    // QAPQueryTask(): An AsyncTask that runs in the background to
    // retrieve the open issues list
    // AsyncTask <params,progress, result>
    // params:the type that is passed into the execute method
    // progress - the type that is used within the task to track progress

    public class GHSQueryTask extends AsyncTask<Void,Void,Void> {

        /** SUBCLASS VARIABLES___________________________ **/
        Boolean isConnected = false;// determine if the device has internet connectivity.
        Boolean isError = false; // determine is an error is encountered

        /** ASYNCTASK METHODS __________________________________________________________________ **/

        // onPreExecute(): This method runs on the UI thread just before the doInBackground method
        // executes.
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            // Displays the progress indicator object.
            // progressIndicator.setVisibility(View.VISIBLE);
        }

        // doInBackground(): This method constantly runs in the background while AsyncTask is
        // running.

        @Override
        protected Void doInBackground(Void... voids) {


            //1. Checks the device's current network and Internet connectivity state.
            isConnected = GHBConnectivity.checkConnectivity(MainActivity.this);

            //2. if connected to Internet, this AsyncTask attempts to retrieve open issues
            // for a particular language

            Log.d(tag,"Is Connected = " + isConnected);
            if (isConnected) {
                Log.d(tag," Connected to Github service success ");
                try {
                    Log.d(tag, "GHSQueryTask(): Retrieving latest openIssues...");
                    retrieveLatestOpenIssues(); // Retrieves the posts from the Retrofit adapter.

                }

                // Exception
                catch (Exception e) {
                    isError = true;
                    Log.e(tag,"GHSQueryTask(): An exception error occured: " + e);
                }

            }
            else {

                isError = true; // Indicates an error has occurred.
              //  Toast.makeText(MainActivity.this, "Your device is not connected to the Internet. Please check your settings and try again.", Toast.LENGTH_SHORT).show();
            }

            return null;
        }

        // onPostExecute(): This method runs on the UI thread after the doInBackground operation has
        // completed.
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            // If the AsyncTask was not cancelled, the recycler view is updated, as long as there
            // were no errors with the request.
            Log.d(tag,"on post execute - async task");
            Log.d(tag,"is not cancelled = " + isCancelled());
            if (!isCancelled()) {
                // Runs on the UI thread.
                Log.d(tag," async task is not cancelled ");
                runOnUiThread(new Runnable() {

                    // Updates the layout view, as long as an error was not encountered.
                    public void run() {
                        //  if (!isError) {
                        Log.d(tag, " no error - updateView()");


                        //updateView(true);
                        // }
                    }//run()
                });//runOnUiThread()
            }
        }//onPostExecute()
    }//AsyncTask


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /* to call the event, we need to register our mainactivity.java with the
    bus so this activity can send and receive events from the bus
     */
    @Override
    protected void onResume() {
        super.onResume();
        recyclerView.setAdapter(recyclerViewAdapter);
        mBus.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mBus.unregister(this);
    }
}
