package Bus;

import com.squareup.otto.ThreadEnforcer;
import com.squareup.otto.Bus;

/**
 * Created by Anu on 3/17/16.
 */
// we want to prohibit inheritance of this class for security reasons
// we are going to add final keyword

// This class will give the instance of the bus
public final class BusProvider {

    // Otto post event
    private static final Bus BUS = new Bus(ThreadEnforcer.ANY);

    public static Bus getInstance() {
        return BUS;
    }
    private BusProvider() {
        
    }
}
