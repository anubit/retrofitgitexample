package Interface;

import android.util.Log;

import javax.security.auth.callback.Callback;

import ex.retrofitgitexample.model.GitHubResponse;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;


/**
 * Created by Anu on 3/14/16.
 * Retrofit 2:
 * retrofit turns your http api into java interface
 * ref: https://futurestud.io/blog/retrofit-synchronous-and-asynchronous-requests
 */
public interface MyRetrofitEndPointInterfaceGitHub {


    /*
     we're searching for repositories with the word tetris in the name, the description, or the README.
     We're limiting the results to only find repositories where the primary language is Assembly.
     We're sorting by stars in descending order, so that the most popular repositories appear first
     in the search results.
      //https://api.github.com/search/repositories?q=tetris+language:assembly&sort=stars&order=desc
     */


    /* examples of call request for differernt websites
     @GET("/posts/stream/global")
     Call<Post> getLatestPosts();
     Call<List<Repo>> listRepos(@Path("user") String user);

     @GET("/2.2/questions?order=desc&sort=creation&site=stackoverflow")
     Call<StackOverflowQuestions> loadQuestions(@Query("tagged") String tags);

      http://api.yelp.com/v2/search?term=food&location=San+Francisco
      @GET("/search/repositories?q=${term}+language:${language}&sort=stars&order=desc")
   */

    /* API call will be made asynchoronously and Callback interface will be the last line
    * call back to grap the json response
    * CallBack<T> interface uses generics to handle reponse objects
    * This is*/



    @GET("/search/repositories")
    //Call getLatestOpenIssues(@Query("term") String searchTerm, Callback<GitHubResponse> callback);
    Call<GitHubResponse> getLatestOpenIssues(
            @Query("q") String searchTerm);
          //  @Query("language") String language);


            ;  // Version 1 without otto
   // void getLatestOpenIssues(retrofit.Callback<GitHubResponse> callback);

}
